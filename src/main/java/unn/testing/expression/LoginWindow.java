package unn.testing.expression;


import com.google.gson.Gson;
import unn.testing.CustomizableLabel;
import unn.testing.PersonInfo;
import unn.testing.StringStorage;
import unn.testing.model.CustomizableButton;
import unn.testing.model.CustomizableTextField;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.*;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class LoginWindow extends JFrame implements ActionListener {
    private static ExpressionManager myManager;
    private static LoginWindow myFrame;
    private boolean isLevelX;

    private CustomizableLabel nameLabel;
    private JTextField personName;

    private CustomizableLabel ageLabel;
    private JTextField age;

    private CustomizableLabel groupLabel;
    private JTextField groupName;

    private CustomizableLabel quantityLabel;
    private JTextField questionQuantity;

    private CustomizableLabel levelLabel;
    private JRadioButton levelX;
    private JRadioButton levelY;

    private JButton nextWindowButton;

    private JFileChooser myFileChooser;
    private JButton browseFileButton;

    private CustomizableLabel sourceLabel;
    private JTextField sourceFileName;


    @SuppressWarnings("unused")
    private void setTexts() {
        personName.setText("1");
        age.setText("10");
        groupName.setText("11bi-1");
        questionQuantity.setText("2");
//		sourceFileName.setText("1_old.txt");
        sourceFileName.setText("fail.txt");
    }

    public LoginWindow(StringStorage data) {
        isLevelX = true;

        nameLabel = new CustomizableLabel(data.getEnterName());
        personName = new CustomizableTextField(null);

        ageLabel = new CustomizableLabel(data.getEnterAge());
        age = new CustomizableTextField(null);

        groupLabel = new CustomizableLabel(data.getEnterGroupName());
        groupName = new CustomizableTextField(null);

        quantityLabel = new CustomizableLabel(data.getEnterQuantity());
        questionQuantity = new CustomizableTextField(null);

        nextWindowButton = new CustomizableButton("Поехали!");

        myFileChooser = new JFileChooser();
        myFileChooser.setFileSelectionMode(JFileChooser.FILES_ONLY);

        sourceLabel = new CustomizableLabel(data.getEnterFile());
        sourceFileName = new CustomizableTextField("deutsche.txt");
        browseFileButton = new CustomizableButton("Browse file..");
        browseFileButton.addActionListener(this);


        final Container pane = this.getContentPane();

        final JPanel myPanel = new JPanel();
        GridLayout myLayout = new GridLayout(4, 2, 20, 20);
        myPanel.setLayout(myLayout);

        myPanel.add(nameLabel);
        myPanel.add(personName);
        myPanel.add(ageLabel);
        myPanel.add(age);
        myPanel.add(groupLabel);
        myPanel.add(groupName);
        myPanel.add(quantityLabel);
        myPanel.add(questionQuantity);

        final JPanel dirPanel = new JPanel();
        dirPanel.setLayout(new GridLayout(4, 3, 20, 20));

        dirPanel.add(sourceLabel);
        dirPanel.add(sourceFileName);
        dirPanel.add(browseFileButton);


        dirPanel.add(new JPanel());
        dirPanel.add(new JPanel());
        dirPanel.add(new JPanel());

        dirPanel.add(new JPanel());
        dirPanel.add(nextWindowButton);
        dirPanel.add(new JPanel());

        dirPanel.add(new JPanel());
        dirPanel.add(new JPanel());
        dirPanel.add(new JPanel());

        pane.setLayout(new GridLayout(2, 1, 20, 20));
        pane.add(myPanel);
        pane.add(dirPanel);

        nextWindowButton.addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent arg0) {
                if (checkValues())
                    beginTesting();
            }


        });
//        setTexts();
    }


    private static final long serialVersionUID = 292804178476994007L;

    public static void main(String args[]) throws IOException {
        InputStream v = ClassLoader.getSystemResourceAsStream("info.json");
        BufferedReader br = new BufferedReader(new InputStreamReader(v, "windows-1251"));

        String sCurrentLine;
        String resLine = "";

        while ((sCurrentLine = br.readLine()) != null) {
            resLine += sCurrentLine;
        }

        StringStorage data = new Gson().fromJson(resLine, StringStorage.class);

//        GraphicsDevice myDevice = GraphicsEnvironment.getLocalGraphicsEnvironment().getDefaultScreenDevice();
        myFrame = new LoginWindow(data);

        myFrame.setExtendedState(JFrame.MAXIMIZED_BOTH);
//        myFrame.setUndecorated(true);
        myFrame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);

        myFrame.setVisible(true);
//        myDevice.setFullScreenWindow(myFrame);
    }

    private void beginTesting() {
        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        Date date = new Date();
        PersonInfo person = new PersonInfo(personName.getText(),
                age.getText(),
                groupName.getText(),
                dateFormat.format(date),
                isLevelX);
        myManager = new ExpressionManager(person);
        List<String> lines;
        try {
            myManager.readExpressionsFromTextFile(sourceFileName.getText());

            myManager.getRandomExpressions(Integer.parseInt(questionQuantity.getText()));

            myFrame.setVisible(false);
            myManager.setLevelX(true);
            myManager.setCurrentScore(0);
            myManager.createNewInstructionWindow(myManager.getInstruction());
        } catch (IOException e) {
            System.out.println("Wrong file");

            JOptionPane.showMessageDialog(myFrame,
                    "The file is not correct - not for testing!",
                    "Error in file",
                    JOptionPane.ERROR_MESSAGE);

        }
    }

    private boolean checkValues() {
        return !(personName.getText().equals("") || age.getText().equals("")
                || groupName.getText().equals("") || questionQuantity.getText().equals(""));
    }

    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == browseFileButton) {
            int returnVal = myFileChooser.showOpenDialog(LoginWindow.this);

            if (returnVal == JFileChooser.APPROVE_OPTION) {
                File file = myFileChooser.getSelectedFile();
                //This is where a real application would open the file.
                System.out.println("Opening: " + file.getName());
                sourceFileName.setText(file.getAbsolutePath());
            }
            return;
        }

            isLevelX = true;
    }
}
