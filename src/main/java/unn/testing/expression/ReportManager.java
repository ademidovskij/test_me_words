package unn.testing.expression;

import unn.testing.model.Expression;
import unn.testing.report.ReportUtils;

import java.io.IOException;
import java.util.ArrayList;

public class ReportManager {
    public static void makeReport(String name,
                                  String age,
                                  String group,
                                  String string,
                                  ArrayList<Expression> questions,
                                  ArrayList<Integer> results,
                                  ArrayList<Integer> times,
                                  ArrayList<Integer> timesThinking,
                                  boolean isLevelX,
                                  int j,
                                  ArrayList<String> option) throws IOException {
        StringBuilder myBuilder = new StringBuilder();

        myBuilder.append("Name: \t" + name + '\n');
        myBuilder.append("Age: \t" + age + '\n');
        myBuilder.append("Group: \t" + group + '\n');
        myBuilder.append("Date: \t" + string + '\n');
        myBuilder.append("Number of questions: \t" + questions.size() + '\n');
        myBuilder.append("Questions: " + '\n');
        for (int i = 0; i < questions.size(); ++i) {
            myBuilder.append(i + 1 + "" + questions.get(i).getExpression() + '\n');
            String tmpRes = results.get(i) == 1 ? "\t Correct: 1\n" : "\t Correct: 0\n";
            myBuilder.append(tmpRes);
            myBuilder.append("\t Word: " + (String) option.get(i) + "\n");
            myBuilder.append("\t Time before space pressed: " + timesThinking.get(i) + "\n");
            myBuilder.append("\t Time after space pressed: " + times.get(i) + "\n");
        }

        ReportUtils.writeReport(myBuilder.toString(), name, isLevelX);
    }
}
