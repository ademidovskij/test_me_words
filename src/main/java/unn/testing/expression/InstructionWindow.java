package unn.testing.expression;

import unn.testing.CustomizableLabel;
import unn.testing.model.CustomizableTextField;
import unn.testing.model.Expression;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

public class InstructionWindow extends JFrame {


    private CustomizableLabel firstPartLabel;
    private JButton nextButton;
    private CustomizableLabel myScore;
    private String chosenOption;
    private JPanel myPanel;
    private Container pane;
    private CustomizableLabel instructionLabel;

    public InstructionWindow() {
        super();
    }

    public void setNewContent(String instruction, ExpressionManager expressionManager) {

        pane = this.getContentPane();
        pane.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
//        this.setExtendedState(JFrame.MAXIMIZED_BOTH);
        removeAllFromPane();

        instructionLabel = new CustomizableLabel(instruction, 32);

        pane.setLayout(new GridLayout(1, 1));
        pane.add(instructionLabel);
        this.setFocusable(true);
        this.requestFocus();
        this.addKeyListener(expressionManager);

        this.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        //this.pack();
        this.setVisible(true);
        this.setContentPane(pane);
    }

    public void removeAllFromPane() {
        if (myPanel != null) {
            pane.removeAll();
            repaint();
            revalidate();
        } else
            myPanel = new JPanel();
    }
}
