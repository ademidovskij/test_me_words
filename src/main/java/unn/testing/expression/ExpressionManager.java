package unn.testing.expression;

import unn.testing.PersonInfo;
import unn.testing.ResultWindow;
import unn.testing.model.Expression;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.io.BufferedReader;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;


public class ExpressionManager implements ActionListener, KeyListener {

    private ArrayList<String> myList;
    private ArrayList<Expression> randomList;
    private ArrayList<Integer> results;
    private ArrayList<Integer> times;
    private ArrayList<Integer> timesThinking;
    private int currentIndex;
    private ExpressionWindow currentQuestionWindow;
    private Expression currentQuestion;
    private boolean isLevelX;
    private int currentScore;
    private long currentTime;
    private PersonInfo myPerson;
    private ResultWindow resW;
    private String instruction = "";
    private InstructionWindow currentInstructionWindow;
    private ArrayList<String> chosenOptions;
    private boolean isTransferAllowed = false;
    private long timeThinking;

    public ExpressionManager(PersonInfo person){
        myPerson = person;
        myList = new ArrayList<>();
        randomList = new ArrayList<>();
        results = new ArrayList<>();
        times = new ArrayList<>();
        timesThinking = new ArrayList<>();
        currentIndex = 0;
        isLevelX = true;
        currentTime = 0;
        chosenOptions = new ArrayList<>();
    }

    private ArrayList<String> getChosenOptions(){
        return chosenOptions;
    }

    public List<String> readExpressionsFromTextFile(String fileName) throws IOException{
        final Path fullPath = Paths.get(fileName);
        myList = new ArrayList<>();
        //Charset charset = Charset.forName("windows-1251");
        Charset charset = Charset.forName("UTF-8");
        try (BufferedReader reader = Files.newBufferedReader(fullPath, charset)) {
            String line;
            int i = 0;
            while ((line = reader.readLine()) != null) {
                if (i == 0)
                {
                    if (!line.equals("Test file"))
                        throw new IOException("Wrong file!");
                    else
                    {
                        i = -1;
                        continue;
                    }
                }
                String [] res;
                if (line.matches("\\d+.*")){
                    res = line.split("\\d+\\.");
                    if (res.length > 1) {
                        myList.add(res[1]);
                    }
                } else {
                    res = line.split("#");
                    if (res.length > 1) {
                        instruction += res[1] + "\n";
                    } else {
                        instruction += "\n";
                    }
                }
            }
        }
        return myList;
    }


    public void getRandomExpressions(final int quantity){
        if (myList == null){
            return;
        }
        ArrayList<Integer> usedNumbers = new ArrayList<Integer>();
        for (int i = 0; i < quantity; i++){
            int randInt = 0;
            while (true){
                Random r = new Random();
                randInt = r.nextInt(myList.size() + 1);
                if (!usedNumbers.contains(randInt) && randInt < myList.size())
                {
                    break;
                }

            }

            randomList.add(parseStringWithExpression(myList.get(randInt)));
            usedNumbers.add(randInt);
        }
        currentIndex = 0;
    }

    public ArrayList<Expression> getRandomList() {
        return randomList;
    }

    public Expression parseStringWithExpression(String str){
        Expression newExpression = new Expression();
        String[] qA = str.trim().split("\\|");
        newExpression.setExpression(qA[0]);
        String[] answers = qA[1].split("<");
        final String answer = answers[answers.length-1].split(">")[0];
        newExpression.setRightAnswer(this.checkAndReplaceUmlauts(answer));
        return newExpression;
    }

    private String checkAndReplaceUmlauts(String input){
        //replace all lower Umlauts
        String output = input.replace("ü", "ue")
                .replace("ö", "oe")
                .replace("ä", "ae")
                .replace("ß", "ss");

        //first replace all capital umlaute in a non-capitalized context (e.g. Übung)
        output = output.replace("Ü(?=[a-zäöüß ])", "Ue")
                .replace("Ö(?=[a-zäöüß ])", "Oe")
                .replace("Ä(?=[a-zäöüß ])", "Ae");

        //now replace all the other capital umlaute
        output = output.replace("Ü", "UE")
                .replace("Ö", "OE")
                .replace("Ä", "AE");
        return output;
    }

    public void createNewExpressionWindow(Expression expression) {
        isTransferAllowed = false;
        currentQuestion = expression;
        currentTime = System.nanoTime();
        timeThinking = System.nanoTime();
        currentIndex++;
        if (currentQuestionWindow == null)
        {
            currentQuestionWindow = new ExpressionWindow();
        }
        currentQuestionWindow.setNewContent(currentScore, expression, this);

    }

    public void createNewInstructionWindow(String instruction) {
        currentInstructionWindow = new InstructionWindow();
        currentInstructionWindow.setNewContent(instruction, this);
        isTransferAllowed = true;
        GraphicsDevice myDevice = GraphicsEnvironment.getLocalGraphicsEnvironment().getDefaultScreenDevice();
        myDevice.setFullScreenWindow(this.currentInstructionWindow);
    }


    @Override
    public void actionPerformed(ActionEvent arg0) {
        String chosenOption = currentQuestionWindow.getInputWord();
        this.chosenOptions.add(chosenOption);
        if (chosenOption == null || chosenOption.equals("")){
            return;
        }
        else{
            currentQuestionWindow.removeAllFromPane();
        }

        String correctAnswer = currentQuestion.getRightAnswer();
        chosenOption = chosenOption.replaceAll("<.*?>", "").trim();
        boolean isCorrect = false;
        if (chosenOption.equals(correctAnswer)){
            isCorrect = true;
        }

        if (isCorrect)
            results.add(1);
        else
            results.add(0);

        long time = System.nanoTime() - currentTime;
        times.add((int) (time/1000000));
        System.out.println ("Time spent: " + (int) (time/1000000));

        try {
            nextExpression();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    public void nextExpression() throws IOException {
        if (randomList == null || randomList.size() == 0){
            return;
        } else if (currentIndex >= randomList.size()) {
            ReportManager.makeReport(myPerson.getName(),
                    myPerson.getAge(),
                    myPerson.getGroup(),
                    myPerson.getDate(),
                    this.getRandomList(),
                    this.getResults(),
                    this.getTimes(),
                    this.getTimesThinking(),
                    myPerson.isLevelX(),
                    this.getCurrentScore(),
                    this.getChosenOptions());
            System.exit(0);
        } else {
            this.createNewExpressionWindow(randomList.get(currentIndex));
        }
    }

    public boolean isLevelX() {
        return isLevelX;
    }

    public void setLevelX(boolean isLevelX) {
        this.isLevelX = isLevelX;
    }

    public int getCurrentScore() {
        return currentScore;
    }

    public ArrayList<Integer> getResults() {
        return results;
    }

    public ArrayList<Integer> getTimes() {
        return times;
    }

    public ArrayList<Integer> getTimesThinking() {
        return this.timesThinking;
    }

    public void setCurrentScore(int currentScore) {
        this.currentScore = currentScore;
    }

    @Override
    public void keyTyped(KeyEvent e) {

    }

    @Override
    public void keyReleased(KeyEvent e) {

    }

    @Override
    public void keyPressed(KeyEvent e) {
        if (e.getKeyCode() == KeyEvent.VK_ENTER && this.isTransferAllowed) {
            this.createNewExpressionWindow((Expression)this.getRandomList().get(0));
            this.isTransferAllowed = false;
        } else if(e.getKeyCode() == 32 && !this.isTransferAllowed) {
            this.currentQuestionWindow.setTextFieldVisible(true);
            long time = System.nanoTime() - this.timeThinking;
            this.timesThinking.add((int)(time / 1000000L));
            this.currentTime = System.nanoTime();
            this.isTransferAllowed = true;
        }
    }

    public String getInstruction() {
        return instruction;
    }
}
