package unn.testing.expression;

import unn.testing.CustomizableLabel;
import unn.testing.model.CustomizableTextField;
import unn.testing.model.Expression;

import javax.swing.*;
import javax.swing.border.Border;
import javax.swing.text.BadLocationException;
import javax.swing.text.SimpleAttributeSet;
import javax.swing.text.StyleConstants;
import javax.swing.text.StyledDocument;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class ExpressionWindow extends JFrame implements ActionListener {
    private JTextPane firstPartLabel;
    private JButton nextButton;
    private CustomizableLabel myScore;
    private String chosenOption;
    private JPanel myPanel;
    private Container pane;

    private String chosenWord;
    private CustomizableTextField wordTF;

    public ExpressionWindow() {
        super();
    }

    public void setNewContent(int score, Expression e, ExpressionManager parent) {
        pane = this.getContentPane();
        pane.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
        this.setExtendedState(JFrame.MAXIMIZED_BOTH);
        removeAllFromPane();

        myScore = new CustomizableLabel(String.valueOf(score));

        int index = e.getExpression().indexOf("###");
        String str;
        if(index > -1) {
            String[] document = e.getExpression().split("###");
            str = this.adaptStr(document[0] + "___________________" + document[1], "\n");
            str = String.join("\n", str.split("\\\\n"));
        } else {
            str = e.getExpression();
        }

        this.firstPartLabel = new JTextPane();
        this.firstPartLabel.setEditable(false);
        this.firstPartLabel.setBackground(Color.BLACK);

        StyledDocument document1 = (StyledDocument)this.firstPartLabel.getDocument();
        SimpleAttributeSet keyWord = new SimpleAttributeSet();
        StyleConstants.setForeground(keyWord, Color.WHITE);
        StyleConstants.setBold(keyWord, true);
        StyleConstants.setAlignment(keyWord, 1);
        StyleConstants.setFontSize(keyWord, 42);
        StyleConstants.setFontFamily(keyWord, "Serif");
        document1.setParagraphAttributes(0, document1.getLength(), keyWord, false);

        try {
            document1.insertString(document1.getLength(), "\n\n\n\n\n" + str, keyWord);
        } catch (BadLocationException var13) {
            var13.printStackTrace();
        }

        JScrollPane myPane = new JScrollPane(this.firstPartLabel);
        myPane.setPreferredSize(new Dimension(600, 600));
        myPane.setViewportBorder((Border)null);
        myPane.setBorder((Border)null);
        this.nextButton = new JButton("Next");
        this.wordTF = new CustomizableTextField((String)null);
        this.wordTF.setSize(new Dimension(20, 10));
        this.wordTF.setBackground(Color.BLACK);
        this.wordTF.setForeground(Color.WHITE);
        this.wordTF.setCaretColor(Color.WHITE);

        JPanel myOptionPanel = new JPanel();
        myOptionPanel.setBackground(Color.BLACK);
        myOptionPanel.setLayout(new GridLayout(1, 3));
        JPanel tmp1 = new JPanel();
        tmp1.setBackground(Color.BLACK);
        myOptionPanel.add(tmp1);
        myOptionPanel.add(this.wordTF);
        JPanel tmp2 = new JPanel();
        tmp2.setBackground(Color.BLACK);
        myOptionPanel.add(tmp2);

        JPanel tmp3 = new JPanel();
        tmp3.setBackground(Color.BLACK);

        this.pane.setLayout(new BorderLayout());
        this.pane.add(myPane, BorderLayout.NORTH);
        this.pane.add(tmp3, BorderLayout.CENTER);
        this.pane.add(myOptionPanel, BorderLayout.SOUTH);
        this.nextButton.addActionListener(parent);
        this.wordTF.addActionListener(parent);

        myPane.setFocusable(true);
        myPane.requestFocusInWindow();
        myPane.addKeyListener(parent);

        this.wordTF.setVisible(false);
        this.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        this.setVisible(true);
        this.setContentPane(this.pane);

        GraphicsDevice myDevice = GraphicsEnvironment.getLocalGraphicsEnvironment().getDefaultScreenDevice();
        myDevice.setFullScreenWindow(this);
    }

    @Override
    public void setVisible(boolean value) {
        super.setVisible(value);
        wordTF.requestFocusInWindow();
    }


    @Override
    public void actionPerformed(ActionEvent e) {
        chosenOption = ((JRadioButton) (e.getSource())).getText();
        chosenWord = ((JTextField) (e.getSource())).getText();
    }

    public String getChosenOption() {
        return chosenOption;
    }

    public String getInputWord() {
        chosenWord = wordTF.getText();
        return chosenWord;
    }

    private String adaptStr(String mystr, String delimiter) {
        String str = "";
        if (mystr.length() > 50) {
            str = delimiter.equals("<br>")?"<html>":"";
            String[] arr = mystr.split(" ");
            int mean = arr.length / 2;

            for (int j = 0; j < arr.length; j++) {
                str += " " + arr[j];
                if (j == mean) {
                    str += delimiter;

                }
            }

            if (delimiter.equals("<br>")){
                str += "</html>";
            }
        }
        if (str.equals("")){
            return mystr;
        }
        return str;
    }

    public void removeAllFromPane() {
        if (myPanel != null) {
            pane.removeAll();
            //pane.removeAll();
            repaint();
            revalidate();
        } else
            myPanel = new JPanel();
    }

    public void setTextFieldVisible(boolean b) {
        this.wordTF.setVisible(true);
        this.wordTF.requestFocusInWindow();
    }
}
