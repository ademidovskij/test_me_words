package unn.testing.report;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;

public class ReportUtils {
    public static void writeReport(String content, String fileName, boolean isLevelX) throws IOException {
        System.out.println(content);
        String str = isLevelX ? "+" : "-";
        File f = new File(fileName + "_" + str + ".txt");
        f.createNewFile();
        PrintWriter writer = new PrintWriter(f);
        writer.println(content);
        writer.close();
    }
}
