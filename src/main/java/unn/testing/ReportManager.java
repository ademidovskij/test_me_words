package unn.testing;

import unn.testing.report.ReportUtils;

import java.io.IOException;
import java.util.ArrayList;

public class ReportManager {
    public static void makeReport(String name,
                                  String age,
                                  String group,
                                  String string,
                                  ArrayList<Question> questions,
                                  ArrayList<Integer> results,
                                  ArrayList<Integer> times,
                                  boolean isLevelX,
                                  int j) throws IOException {
        StringBuilder myBuilder = new StringBuilder();

        myBuilder.append("Name: \t" + name + '\n');
        myBuilder.append("Age: \t" + age + '\n');
        myBuilder.append("Group: \t" + group + '\n');
        myBuilder.append("Date: \t" + string + '\n');
        myBuilder.append("Number of questions: \t" + questions.size() + '\n');
        if (isLevelX)
            myBuilder.append("Level: \t" + "X" + '\n');
        else
            myBuilder.append("Level: \t" + "Y" + '\n');
        myBuilder.append("Final score: \t" + j + '\n');
        myBuilder.append("Questions: " + '\n');
        for (int i = 0; i < questions.size(); ++i) {
            myBuilder.append(i + 1 + "" + questions.get(i).getQuestion() + '\n');
            if (results.get(i) == 1)
                myBuilder.append("\t Correct: 1\n");
            else
                myBuilder.append("\t Correct: 0\n");
            myBuilder.append("\t Time spent: " + times.get(i) + "\n");
        }

        ReportUtils.writeReport(myBuilder.toString(), name, isLevelX);
    }
}
