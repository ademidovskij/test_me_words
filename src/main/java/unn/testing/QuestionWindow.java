package unn.testing;

import unn.testing.model.Expression;

import java.awt.ComponentOrientation;
import java.awt.Container;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.WindowConstants;

public class QuestionWindow extends JFrame implements ActionListener{

	
	private CustomizableLabel questionLabel;
	private JButton nextButton;
	private CustomizableLabel myScore;
	private String chosenOption;
	private JPanel myPanel;
	private Container pane;
	
	
	private static final long serialVersionUID = -1118838009505188777L;
	
	public QuestionWindow(){
		super();
	}

	public void setNewContent (int score, Question e, QuestionManager parent)
	{
		
		pane = this.getContentPane();
		pane.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
		this.setExtendedState(JFrame.MAXIMIZED_BOTH);
		removeAllFromPane();
		
		myScore = new CustomizableLabel (String.valueOf(score));
		
		questionLabel = new CustomizableLabel(adaptStr(e.getQuestion(),"\n"), true);
		
		nextButton = new JButton("Next");
		
		
		
		
		pane.setLayout(new GridBagLayout());
		GridBagConstraints c = new GridBagConstraints();
		c.fill = GridBagConstraints.HORIZONTAL;
		c.fill = GridBagConstraints.VERTICAL;
		c.gridx = 7;
		c.gridy = 0;
		c.weightx =1;
		c.weighty = 0.5;
		pane.add(new CustomizableLabel("Score"), c);
		
		c.fill = GridBagConstraints.HORIZONTAL;
		c.fill = GridBagConstraints.VERTICAL;
		c.gridx = 7;
		c.gridy = 1;
		pane.add(myScore, c);
		
		c.fill = GridBagConstraints.HORIZONTAL;
		
		c.weighty = 1.0;   //request any extra vertical space
		c.gridx = 0;       //aligned with button 2
		c.gridwidth = 7;   //2 columns wide
		c.gridy = 2;       //third row
		
		pane.add(questionLabel, c);
		
		ButtonGroup group = new ButtonGroup();
		@SuppressWarnings("unchecked")
		ArrayList<String> options = (ArrayList<String>) e.getOptions().clone();
		for (int i = 0 ; i < options.size(); i++){
			String str = e.getOptions().get(i);
			
			str = adaptStr(str,"<br>");
			JRadioButton newButton = new JRadioButton(str);
			newButton.setFont(new Font("Serif", Font.PLAIN, CustomizableLabel.currentSize));
			
			newButton.addActionListener(this);
			//optionsRadioButtons.add(newButton);
			//c.fill = GridBagConstraints.HORIZONTAL;
			//c.weightx = 0.5;
			c.weighty = 1;
			c.gridx = 0;
			c.gridy += 1;
			c.gridwidth = 7;
			pane.add(newButton, c);
			group.add(newButton);
		}
		
		c.fill = GridBagConstraints.HORIZONTAL;
		c.ipady = 40;       //reset to default
		c.weighty = 1.0;   //request any extra vertical space
		c.anchor = GridBagConstraints.PAGE_END; //bottom of space
		c.insets = new Insets(10,0,0,0);  //top padding
		c.gridx = 1;       //aligned with button 2
		c.gridwidth = 7;   //2 columns wide
		c.gridy += 1;       //third row
		//c.gridheight = 2;
		pane.add(nextButton,c);
		nextButton.addActionListener(parent);
		
		
		//this.setUndecorated(true);

		this.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
		//this.pack();
		this.setVisible(true);
		this.setContentPane(pane);
	}

	
	@Override
	public void actionPerformed(ActionEvent e) {
		chosenOption = ((JRadioButton)(e.getSource())).getText();
		System.out.println(chosenOption);
	}

	public String getChosenOption() {
		return chosenOption;
	}
	
	private String adaptStr (String mystr,String delimiter)
	{
		String str = "";
		if (mystr.length() > 50)
		{
			if (delimiter.equals("<br>"))
				str = "<html>";
			else
				str = "";
			String [] arr = mystr.split(" ");
			int mean = arr.length/2;
			
			
			for (int j = 0; j < arr.length; j++){
				str += " " + arr[j];
				if (j == mean){
					str += delimiter;
					
				}
			}
			
			if (delimiter.equals("<br>"))
				str += "</html>";
		}
		return str.equals("")?mystr:str;
	}
	
	public void  removeAllFromPane (){
		if (myPanel != null){
			pane.removeAll();
			//pane.removeAll();
			repaint();
			revalidate();
		}
		else {
			myPanel = new JPanel();
		}
	}
}
