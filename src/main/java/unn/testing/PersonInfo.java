package unn.testing;

public class PersonInfo {
	private String name;
	private String age;
	private String group;
	private String date;
	private boolean isLevelX;
	
	public PersonInfo (String nm, String ag, String gr, String dt, boolean isX){
		setName(nm);
		setAge(ag);
		setGroup(gr);
		setDate(dt);
		setLevelX(isX);
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAge() {
		return age;
	}

	public void setAge(String age) {
		this.age = age;
	}

	public String getGroup() {
		return group;
	}

	public void setGroup(String group) {
		this.group = group;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public boolean isLevelX() {
		return isLevelX;
	}

	public void setLevelX(boolean isLevelX) {
		this.isLevelX = isLevelX;
	}

}
