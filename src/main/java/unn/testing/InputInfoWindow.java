package unn.testing;



import com.google.gson.Gson;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.*;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.swing.BoxLayout;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextField;
import javax.swing.WindowConstants;

public class InputInfoWindow extends JFrame implements ActionListener{
	private static QuestionManager myManager;
	private static InputInfoWindow myFrame;
	/**
	 * 
	 */
	private boolean isLevelX;
	
	private CustomizableLabel nameLabel;
	private JTextField personName;
	
	private CustomizableLabel ageLabel;
	private JTextField age;

	private CustomizableLabel groupLabel;
	private JTextField groupName;
	
	private CustomizableLabel quantityLabel;
	private JTextField questionQuantity;
	
	private CustomizableLabel levelLabel;
	private JRadioButton levelX;
	private JRadioButton levelY;
	
	private JButton nextWindowButton;
	
	private JFileChooser myFileChooser;
	private JButton browseFileButton;
	
	private CustomizableLabel sourceLabel;
	private JTextField sourceFileName;
	
	
	
	@SuppressWarnings("unused")
	private void setTexts ()
	{
		personName.setText("1");
		age.setText("10");
		groupName.setText("11bi-1");
		questionQuantity.setText("2");
//		sourceFileName.setText("1_old.txt");
		sourceFileName.setText("deutsche.txt");
	}
	public InputInfoWindow (StringStorage stringStorage){
		isLevelX = true;
		
		GridLayout myLayout = new GridLayout(6, 2, 20,60);
		
		nameLabel = new CustomizableLabel(stringStorage.getEnterName());
		personName = new JTextField();
		
		ageLabel = new CustomizableLabel(stringStorage.getEnterAge());
		age = new JTextField();
		
		groupLabel = new CustomizableLabel(stringStorage.getEnterGroupName());
		groupName = new JTextField();
		
		quantityLabel = new CustomizableLabel(stringStorage.getEnterQuantity());
		questionQuantity = new JTextField();
		
		levelLabel = new CustomizableLabel(stringStorage.getEnterTestingType());
		levelX = new JRadioButton("X");
		levelY = new JRadioButton("Y");
		
		nextWindowButton = new JButton("Go");
		
		myFileChooser = new JFileChooser();
		myFileChooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
		//myDirFileChooser = new JFileChooser();
		//myDirFileChooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
		
		sourceLabel = new CustomizableLabel(stringStorage.getEnterFile());
		sourceFileName = new JTextField("deutsche.txt");
		browseFileButton = new JButton("Browse file..");
		browseFileButton.addActionListener(this);
		
		
		
		final Container pane = this.getContentPane();
		final JPanel myPanel = new JPanel();
		
		myPanel.setLayout(myLayout);
		
		myPanel.add(nameLabel);
		myPanel.add(personName);
		myPanel.add(ageLabel);
		myPanel.add(age);
		myPanel.add(groupLabel);
		myPanel.add(groupName);
		myPanel.add(quantityLabel);
		myPanel.add(questionQuantity);
		myPanel.add(levelLabel);
		
		
		final JPanel levelPanel = new JPanel();
		levelPanel.setLayout(new GridLayout(1, 1));
		levelPanel.add(levelX);
		levelPanel.add(levelY);
		
		myPanel.add(levelPanel);
		
		final JPanel dirPanel = new JPanel();
		dirPanel.setLayout(new GridLayout(1, 2));
		dirPanel.add(sourceLabel);
		dirPanel.add(sourceFileName);
		dirPanel.add(browseFileButton);
		
		
		
		//myPanel.add(dirPanel);
		
		
		//myPanel.add(nextWindowButton);
		 
		pane.add(myPanel, BorderLayout.NORTH);
		BoxLayout mainLayout = new BoxLayout(pane, BoxLayout.Y_AXIS);
		pane.setLayout(mainLayout);
		pane.add(dirPanel, BorderLayout.CENTER);
		pane.add(nextWindowButton, BorderLayout.SOUTH);
		ButtonGroup myGroup = new ButtonGroup();
		myGroup.add(levelX);
		myGroup.add(levelY);
		
		levelX.addActionListener(this);
		levelY.addActionListener(this);
		
		levelX.setSelected(true);
		
		nextWindowButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if (checkValues())
					beginTesting();
			}
		});
		setTexts();
	}
	
	
	private static final long serialVersionUID = 292804178476994007L;

	public static void main(String args[]) throws IOException {
		String filePath = new File("").getAbsolutePath();
		filePath = filePath.concat("/src/main/resources/info.json");
		FileReader v = new FileReader(filePath);
		BufferedReader br = new BufferedReader(v);

		String sCurrentLine;
		String resLine = "";

		while ((sCurrentLine = br.readLine()) != null) {
			resLine += sCurrentLine;
		}

		StringStorage data = new Gson().fromJson(resLine, StringStorage.class);

		myFrame = new InputInfoWindow(data);

		
		myFrame.setExtendedState(JFrame.MAXIMIZED_BOTH);
		//myFrame.setUndecorated(true);
		myFrame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);

		myFrame.setVisible(true);
	}

	private void beginTesting() {
		DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		Date date = new Date();
		PersonInfo person = new PersonInfo(personName.getText(),
											age.getText(), 
											groupName.getText(), 
											dateFormat.format(date),
											isLevelX);
		myManager = new QuestionManager(person);
		List<String> lines;
		try {
			lines = myManager.readQuestionsFromTextFile(sourceFileName.getText());
			System.out.println(lines);
			
			myManager.getRandomQuestions(Integer.parseInt(questionQuantity.getText()));
			
			myFrame.setVisible(false);
			if (isLevelX)
			{
				myManager.setLevelX(true);
				myManager.setCurrentScore(0);
			}
			else{
				myManager.setLevelX(false);
				myManager.setCurrentScore(50 * Integer.parseInt(questionQuantity.getText()));
			}
			myManager.createNewQuestionWindow(myManager.getRandomList().get(0));
		} catch (IOException e) {
			System.out.println ("Wrong file");
			
			JOptionPane.showMessageDialog(myFrame,
					"The file is not correct - not for testing!",
				    "Error in file",
				    JOptionPane.ERROR_MESSAGE);
		}
		
	}
	
	private boolean checkValues() {
		if (personName.getText().equals("") || age.getText().equals("") 
			|| groupName.getText().equals("") || questionQuantity.equals(""))
			return false;
		return true;
			
		
	}

	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == browseFileButton){
			int returnVal = myFileChooser.showOpenDialog(InputInfoWindow.this);
			 
            if (returnVal == JFileChooser.APPROVE_OPTION) {
                File file = myFileChooser.getSelectedFile();
                //This is where a real application would open the file.
                System.out.println("Opening: " + file.getName());
                sourceFileName.setText(file.getAbsolutePath());
            } 
            return;
		}
		
		final String level = ((JRadioButton)(e.getSource())).getText();
		if (level.equals("X"))
			isLevelX = true;
		else
			isLevelX = false;
	}
}
