package unn.testing;

import java.awt.Container;
import java.awt.Font;
import java.awt.GraphicsEnvironment;

import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;


public class ResultWindow extends  JDialog{
	private static final long serialVersionUID = -5419571192407593292L;

	public ResultWindow (String res){
		final Container pane = this.getContentPane();
		final JPanel myPanel = new JPanel();
		
		JLabel resultLabel = new JLabel(res);
		resultLabel.setFont(new Font("Serif", Font.PLAIN, 145));
		
		
		myPanel.add(resultLabel);
		pane.add(myPanel);
		
		//this.setExtendedState(JDialog.MAXIMIZED_BOTH);
		this.setBounds(GraphicsEnvironment.getLocalGraphicsEnvironment().getMaximumWindowBounds());
		this.setUndecorated(true);
		this.setVisible(true);
	}
}
