package unn.testing;

public class StringStorage {
    private String enterName;
    private String enterAge;
    private String enterGroupName;
    private String enterQuantity;
    private String enterFile;
    private String enterTestingType;

    public String getEnterName() {
        return enterName;
    }

    public String getEnterAge() {
        return enterAge;
    }

    public String getEnterGroupName() {
        return enterGroupName;
    }

    public String getEnterQuantity() {
        return enterQuantity;
    }

    public String getEnterFile() {
        return enterFile;
    }

    public String getEnterTestingType() {
        return enterTestingType;
    }
}
