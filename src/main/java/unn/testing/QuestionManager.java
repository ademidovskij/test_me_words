package unn.testing;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;


public class QuestionManager implements ActionListener{
	
	private ArrayList<String> myList;
	private ArrayList<Question> randomList;
	private ArrayList<Integer> results;
	private ArrayList<Integer> times;
	private int currentIndex;
	private QuestionWindow currentQuestionWindow;
	private Question currentQuestion;
	private boolean isLevelX;
	private int currentScore;
	private long currentTime;
	private PersonInfo myPerson;
	private ResultWindow resW;
	
	public QuestionManager (PersonInfo person){
		myPerson = person;
		myList = new ArrayList<String>();
		randomList = new ArrayList<Question>();
		results = new ArrayList<Integer>();
		times = new ArrayList<Integer>();
		currentIndex = 0;
		isLevelX = true;
		currentTime = 0;
	}
	
	public List<String> readQuestionsFromTextFile (String fileName) throws IOException{
		final Path fullPath = Paths.get(fileName);
		 myList = new ArrayList<String>();
		 // here we need to somehow check for the filetype
		Charset charset = Charset.forName("UTF-8");
		try (BufferedReader reader = Files.newBufferedReader(fullPath, charset)) {
		    String line = null;
		    int i = 0;
		    while ((line = reader.readLine()) != null) {
		        if (i == 0) {
		        	if (!line.equals("Test file"))
		        		throw new IOException("Wrong file!");
		        	else {
		        		System.out.println("Correct file");
		        		 i = -1;
		        		 continue;
		        	}
		        }
		        myList.add(line);
		    }
		} catch (IOException x) {
		    throw x;
		}
		return myList;
	}
	
	
	public void getRandomQuestions (final int quantity){
		if (myList == null){
			return;
		}
		ArrayList<Integer> usedNumbers = new ArrayList<Integer>();
		for (int i = 0; i < quantity; i++){
			int randInt = 0;
			while (true){
				Random r = new Random();
				randInt = r.nextInt(myList.size() + 1);
				if (!usedNumbers.contains(randInt) && randInt < myList.size()) {
					break;
				}
			}
			
			System.out.println(parseStringWithQuestion(myList.get(randInt)));
			randomList.add(parseStringWithQuestion(myList.get(randInt)));
			usedNumbers.add(randInt);
		}
		currentIndex = 0;
	}
	
	public ArrayList<Question> getRandomList() {
		return randomList;
	}

	public Question parseStringWithQuestion (String str){
		Question newQuestion = new Question();
		String[] qA = str.split(":");
		newQuestion.setQuestion(qA[0]);
		String[] answers = qA[1].split(";");
		ArrayList<String> options = new ArrayList<String>();
		for (int i =0; i < answers.length; ++i){
			if (answers[i].contains("!")){
				final int pos = answers[i].indexOf("!");
				answers[i] = answers[i].substring(0, pos)+answers[i].substring(pos+1, answers[i].length());
				newQuestion.setRightAnswer(i);
			}
			answers[i] = answers[i].substring(3,answers[i].length()).trim();
			options.add(answers[i]);
		}
		newQuestion.setOptions(options);
		return newQuestion;
	}


	public void createNewQuestionWindow(Question question) {
		currentQuestion = question;
		
		currentTime = System.nanoTime();
		currentIndex++;
		if (currentQuestionWindow == null)
		{
			currentQuestionWindow = new QuestionWindow();
		}
		currentQuestionWindow.setNewContent(currentScore, question, this);
		
	}


	@Override
	public void actionPerformed(ActionEvent arg0) {
		System.out.println (currentQuestionWindow.getChosenOption());
		
		String chosenOption = currentQuestionWindow.getChosenOption();
		if (chosenOption == null || chosenOption == "")
			return;
		else
			currentQuestionWindow.removeAllFromPane();
			
		String correctAnswer = currentQuestion.getOptions().get(currentQuestion.getRightAnswer());
		chosenOption = chosenOption.replaceAll("<.*?>", "").trim();
		boolean isCorrect = false;
		if (chosenOption.equals(correctAnswer)){
			isCorrect = true;
		}
		
		if (isCorrect)
			results.add(1);
		else
			results.add(0);
		
		long time = System.nanoTime() - currentTime;
		times.add((int) (time/1000000));
		System.out.println ("Time spent: " + (int) (time/1000000));
		//currentQuestionWindow.setVisible(false);
		
		Timer timer = new Timer();
		
		String textForLabel = "";
		if (isCorrect && isLevelX){
			textForLabel = "+50";
			currentScore += 50;
		} else if (!isCorrect && isLevelX){
			textForLabel = "";
		} else if (isCorrect && !isLevelX){
			textForLabel = "";
		} else if (!isCorrect && !isLevelX){
			textForLabel = "-50";
			currentScore -= 50;
		}
	 resW = new ResultWindow(textForLabel);
	 
		timer.schedule(new TimerTask() {
			
			@Override
			public void run() {
				resW.setVisible(false);

				try {
					nextQuestion();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}, 3*1000);
	}
	
	
	public void nextQuestion () throws IOException {
		if (randomList == null || randomList.size() == 0)
			return;
		if (currentIndex >= randomList.size())
		{
			ReportManager.makeReport(myPerson.getName(), 
										myPerson.getAge(),
										myPerson.getGroup(),
										myPerson.getDate(),
										this.getRandomList(), 
										this.getResults(), 
										this.getTimes(), 
										myPerson.isLevelX(),
										this.getCurrentScore());
			System.exit(0);
			return;
			
		}
		createNewQuestionWindow(randomList.get(currentIndex));
	}

	public boolean isLevelX() {
		return isLevelX;
	}

	public void setLevelX(boolean isLevelX) {
		this.isLevelX = isLevelX;
	}

	public int getCurrentScore() {
		return currentScore;
	}

	public ArrayList<Integer> getResults() {
		return results;
	}

	public ArrayList<Integer> getTimes() {
		return times;
	}

	public void setCurrentScore(int currentScore) {
		this.currentScore = currentScore;
	}
}
