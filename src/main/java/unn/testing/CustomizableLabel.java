package unn.testing;


import java.awt.Font;
import java.awt.font.TextAttribute;
import java.util.Hashtable;

import javax.swing.JTextArea;

public class CustomizableLabel extends JTextArea {
    public static int currentSize = 42;
    private static final long serialVersionUID = 7546452351171747097L;

    public CustomizableLabel(String str, boolean isSpecial) {
        super(str);
        Font f = new Font("Serif", 3, currentSize);
        Hashtable<TextAttribute, Integer> map = new Hashtable<TextAttribute, Integer>();
        map.put(TextAttribute.UNDERLINE, TextAttribute.UNDERLINE_ON);
        f = f.deriveFont(map);
        this.setFont(f);
        this.setOpaque(false);
        this.setEditable(false);
        this.setWrapStyleWord(true);
        this.setLineWrap(true);
    }

    public CustomizableLabel(String str) {
        super(str);
        this.setFont(new Font("Serif", 0, currentSize));
        this.configure();
    }

    public CustomizableLabel(String str, int size) {
        super(str);
        this.setFont(new Font("Serif", 0, size));
        this.configure();
    }

    private void configure() {
        this.setOpaque(false);
        this.setEditable(false);
        this.setWrapStyleWord(true);
        this.setLineWrap(true);
    }
}
