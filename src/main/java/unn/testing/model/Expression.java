package unn.testing.model;

public class Expression {
    private String expression;
    private String rightAnswer;

    public String getExpression() {
        return expression;
    }
    public void setExpression(String expression) {
        this.expression = expression;
    }
    @Override
    public String toString() {
        return "Expression [expression=" + expression + ", rightAnswer=" + rightAnswer + "]";
    }
    public String getRightAnswer() {
        return rightAnswer;
    }
    public void setRightAnswer(String rightAnswer) {
        this.rightAnswer = rightAnswer;
    }
}
