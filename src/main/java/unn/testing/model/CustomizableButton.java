package unn.testing.model;

import java.awt.Font;
import java.awt.font.TextAttribute;
import java.util.Hashtable;
import javax.swing.JButton;

public class CustomizableButton extends JButton{
	public static  int currentSize = 40;
	private static final long serialVersionUID = 7546452351171747097L;

	public CustomizableButton(String str, boolean isSpecial)
	{
		super(str);
		Font f = new Font("Serif", Font.PLAIN, currentSize);
		 Hashtable<TextAttribute, Object> map =
		            new Hashtable<TextAttribute, Object>();
		 map.put(TextAttribute.UNDERLINE, TextAttribute.UNDERLINE_ON);

	        f = f.deriveFont(map);
		this.setFont(f);
		this.setOpaque(false);
	}

	public CustomizableButton(String str)
	{
		super(str);
		this.setFont(new Font("Serif", Font.PLAIN, currentSize));
		this.setOpaque(false);
	}
}
