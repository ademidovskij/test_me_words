package unn.testing;

import java.util.ArrayList;

public class Question {
	private String question;
	private ArrayList<String> options;
	private int rightAnswer;
	
	public String getQuestion() {
		return question;
	}
	public void setQuestion(String question) {
		this.question = question;
	}
	public ArrayList<String> getOptions() {
		return options;
	}
	public void setOptions(ArrayList<String> options) {
		this.options = options;
	}
	@Override
	public String toString() {
		return "Question [question=" + question + ", options=" + options
				+ ", rightAnswer=" + rightAnswer + "]";
	}
	public int getRightAnswer() {
		return rightAnswer;
	}
	public void setRightAnswer(int rightAnswer) {
		this.rightAnswer = rightAnswer;
	}
	

}
